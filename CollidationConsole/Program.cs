﻿using CollidationDomain;
using System;

namespace CollidationConsole
{
    class Program
    {
        /// <summary>
        /// Simple console program to ask for the position of the cubes.
        /// Return the results of collidation & intersected volume
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.Write("Welcome to the collidation calculator. \nUnits could be integers or decimal numbers. \n \n");

            Cube cube1 = CreateCube(1);
            Cube cube2 = CreateCube(2);

            if (cube1.Collide(cube2))
                Console.Write("The selected objects collide. \n");
            else
                Console.Write("The selected objects do not collide. \n");

            var intersectedVolume = cube1.IntersectedVolume(cube2);

            Console.Write($"The intersected volume is {intersectedVolume} units. \n");

            Console.Read();
        }



        static Cube CreateCube(int objectNumber)
        {
            decimal x = 0;
            decimal y = 0;
            decimal z = 0;
            decimal edge = 0;

            var errorCount = 0;
            var xValid = false;
            while (!xValid)
            {
                if (errorCount > 0)
                    Console.Write("ERROR: Insert a valid number. \n");
                Console.Write($"Please, insert the X position of the object {objectNumber}:");
                var xLine = Console.ReadLine();
                xLine = xLine.Replace(".", ",");

                xValid = decimal.TryParse(xLine, out x);
                if (!xValid)
                    errorCount++;
            }

            var yValid = false;
            errorCount = 0;
            while (!yValid)
            {
                if (errorCount > 0)
                    Console.Write("ERROR: Insert a valid number. \n");
                Console.Write($"Please, insert the Y position of the object {objectNumber}:");
                var yLine = Console.ReadLine();
                
                if (yLine.Contains("."))
                {
                    yLine = yLine.Replace(".", ",");
                }

                yValid = decimal.TryParse(yLine, out y);
                if (!yValid)
                    errorCount++;
            }

            var zValid = false;
            errorCount = 0;
            while (!zValid)
            {
                if (errorCount > 0)
                    Console.Write("ERROR: Insert a valid number. \n");
                Console.Write($"Please, insert the Z position of the object {objectNumber}:");
                var zLine = Console.ReadLine();

                if (zLine.Contains("."))
                {
                    zLine = zLine.Replace(".", ",");
                }

                zValid = decimal.TryParse(zLine, out z);

                if (!zValid)
                    errorCount++;
            }

            var edgeValid = false;
            errorCount = 0;
            while (!edgeValid)
            {
                if (errorCount > 0)
                    Console.Write("ERROR: Insert a valid number. \n");
                Console.Write($"Please, insert the edge length (units) of the object {objectNumber}:");
                var edgeLine = Console.ReadLine();

                if (edgeLine.Contains("."))
                {
                    edgeLine = edgeLine.Replace(".", ",");
                }

                edgeValid = decimal.TryParse(edgeLine, out edge);

                if (!edgeValid)
                    errorCount++;
            }

            return new Cube(x, y, z, edge);
        }
    }
}
