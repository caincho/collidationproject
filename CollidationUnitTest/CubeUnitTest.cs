﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CollidationDomain;

namespace CollidationUnitTest
{
    [TestClass]
    public class CubeUnitTest
    {
        [TestMethod]
        public void Cube1UnitEdgeReturns1UnitVolumeSucces()
        {
            var aCube = new Cube(0, 0, 0, 1);
            var expectedVolume = 1;

            var realVolume = aCube.Volume();

            Assert.AreEqual(expectedVolume, realVolume);
        }


        [TestMethod]
        public void Cube2UnitsEdgeReturns8VolumeSucces()
        {
            var aCube = new Cube(0, 0, 0, 2);
            var expectedVolume = 8;

            var realVolume = aCube.Volume();

            Assert.AreEqual(expectedVolume, realVolume);
        }

        [TestMethod]
        public void Cube3UnitsEdgeReturns27VolumeSucces()
        {
            var aCube = new Cube(0, 0, 0, 3);
            var expectedVolume = 27;

            var realVolume = aCube.Volume();

            Assert.AreEqual(expectedVolume, realVolume);
        }

        [TestMethod]
        public void Cube4UnitsEdgeReturns65VolumeFail()
        {
            var aCube = new Cube(0, 0, 0, 4);
            var expectedVolume = 65;

            var realVolume = aCube.Volume();

            Assert.AreNotEqual(expectedVolume, realVolume);
        }

    }
}
