﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CollidationDomain;

namespace CollidationUnitTest
{
    [TestClass]
    public class CubicCollidationCalculatorUnitTest
    {


        [TestMethod]
        public void CalculateCubeCollidationOnCornerReturnsFalse()
        {
            var aCube = new Cube(0, 0, 0, 2);
            var otherCube = new Cube(4, 4, 4, 2);
            var expectedIntersectedVolume = false;

            var collide = aCube.Collide(otherCube);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCubeCollidationOnEdgeReturnsFalse()
        {
            var aCube = new Cube(0, 0, 0, 2);
            var otherCube = new Cube(2, 4, 4, 2);
            var expectedIntersectedVolume = false;

            var collide = aCube.Collide(otherCube);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCubeCollidationAllAxisReturnsTrue()
        {
            var aCube = new Cube(0, 0, 0, 2);
            var otherCube = new Cube(1, 1, 1, 2);
            var expectedIntersectedVolume = true;

            var collide = aCube.Collide(otherCube);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCubeCollidationXAxisReturnsTrue()
        {
            var aCube = new Cube(-2, 0, -3, 4);
            var otherCuber = new Cube(1, 1, -3, 4);
            var expectedIntersectedVolume = true;

            var collide = aCube.Collide(otherCuber);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCubeCollidationYAxisReturnsTrue()
        {
            var aCube = new Cube(0, -2, -3, 4);
            var otherCube = new Cube(1, 1, -3, 4);
            var expectedIntersectedVolume = true;

            var collide = aCube.Collide(otherCube);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCubeCollidationZAxisReturnsTrue()
        {
            var aCube = new Cube(-3, 0, -2, 4);
            var otherCube = new Cube(-3, 1, 1, 4);
            var expectedIntersectedVolume = true;

            var collide = aCube.Collide(otherCube);

            Assert.AreEqual(expectedIntersectedVolume, collide);

        }

        [TestMethod]
        public void CalculateCollidationLengthXCollidation()
        {
            var aCube = new Cube(-2, 0, -3, 4);
            var otherCube = new Cube(1, 1, -3, 4);
            var expectedIntersectedLength = 1;

            var xIntersectedLength = aCube.IntersectedLengthX(otherCube);

            Assert.AreEqual(expectedIntersectedLength, xIntersectedLength);

        }

        [TestMethod]
        public void CalculateCollidationLengthYCollidation()
        {
            var aCube = new Cube(0, -2, -3, 4);
            var otherCube = new Cube(1, 0, -3, 4);
            var expectedIntersectedLength = 2;

            var yIntersectedLength = aCube.IntersectedLengthY(otherCube);

            Assert.AreEqual(expectedIntersectedLength, yIntersectedLength);

        }

        [TestMethod]
        public void CalculateCollidationLengtZCollidation()
        {
            var aCube = new Cube(0, 0, 0, 4);
            var otherCube = new Cube(4, -1, 0, 6);
            var expectedVolume = 4;

            var zIntersectedLength = aCube.IntersectedLengthZ(otherCube);

            Assert.AreEqual(expectedVolume, zIntersectedLength);

        }

        [TestMethod]
        public void CalculateVolumeCollidationSucces()
        {
            var aCube = new Cube(0, 0, 0, 4);
            var otherCube = new Cube(4, -1, 0, 6);
            var expectedVolume = 16;

            var intersectedVolume = aCube.IntersectedVolume(otherCube);

            Assert.AreEqual(expectedVolume, intersectedVolume);

        }

        [TestMethod]
        public void CalculateVolumeCollidationFail()
        {
            var aCube = new Cube(4, -1, 0, 6);
            var otherCube = new Cube(4, -1, 0, 6);
            var expectedVolume = 0;

            var intersectedVolume = aCube.IntersectedVolume(otherCube);

            Assert.AreNotEqual(expectedVolume, intersectedVolume);

        }

        [TestMethod]
        public void CalculateVolumeSmallerCollidationSuccess()
        {
            var aCube = new Cube(4, -1, 0, 6);
            var otherCube = new Cube(4, -1, 0, 1);
            var expectedVolume = 1;

            var intersectedVolume = aCube.IntersectedVolume(otherCube);

            Assert.AreEqual(expectedVolume, intersectedVolume);

        }
    }
}
