﻿using System;

namespace CollidationDomain
{
    /// <summary>
    /// Calculates the intersection of a cube or a rectangular object
    /// </summary>
    public abstract class CubicCollidationCalculator : CollidationCalculator
    {
        #region Volume

        public override decimal IntersectedVolume(Object anObject)
        {
            return IntersectedLengthX(anObject) * IntersectedLengthY(anObject) * IntersectedLengthZ(anObject);
        }

        public decimal IntersectedLengthX(Object anObject)
        {
            return Math.Max(0, Math.Min(MaxLengthX(), anObject.MaxLengthX()) - Math.Max(MinLengthX(), anObject.MinLengthX()));
        }

        public decimal IntersectedLengthY(Object anObject)
        {
            return Math.Max(0, Math.Min(MaxLenghtY(), anObject.MaxLenghtY()) - Math.Max(MinLengthY(), anObject.MinLengthY()));
        }

        public decimal IntersectedLengthZ(Object anObject)
        {
            return Math.Max(0, Math.Min(MaxLenghtZ(), anObject.MaxLenghtZ()) - Math.Max(MinLengthZ(), anObject.MinLengthZ()));
        }

        #endregion

        #region Collidation

        public override bool Collide(CollidationCalculator anObject)
        {
            return anObject.CollideCube(this);
        }

        public override bool CollideCube(CubicCollidationCalculator anObject)
        {
            return CollideCubeX(anObject) &&
                   CollideCubeY(anObject) &&
                   CollideCubeZ(anObject);
        }

        public abstract bool CollideCubeX(CubicCollidationCalculator anObject);

        public abstract bool CollideCubeY(CubicCollidationCalculator anObject);

        public abstract bool CollideCubeZ(CubicCollidationCalculator anObject);

        #endregion
    }
}
