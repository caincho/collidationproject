﻿
namespace CollidationDomain
{
    /// <summary>
    /// Abstract class to implement the intersection between any objects
    /// </summary>
    public abstract class CollidationCalculator : Object
    {
        #region Intersection behaviors

        public abstract bool Collide(CollidationCalculator anObject);

        public abstract decimal IntersectedVolume(Object anObject);

        public abstract bool CollideCube(CubicCollidationCalculator anObject);

        #endregion
    }
}
