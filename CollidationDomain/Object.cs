﻿
namespace CollidationDomain
{
    /// <summary>
    /// Abstract class to implement any object
    /// </summary>
    public abstract class Object
    {
        protected decimal _posX;
        protected decimal _posY;
        protected decimal _posZ;
        protected decimal _edge;

        #region Position & Properties

        /// <summary>
        /// Center position on X axis
        /// </summary>
        /// <returns></returns>
        public decimal PosX()
        {
            return this._posX;
        }

        /// <summary>
        /// Center position on Y axis
        /// </summary>
        /// <returns></returns>
        public decimal PosY()
        {
            return this._posY;
        }

        /// <summary>
        /// Center position on Z axis
        /// </summary>
        /// <returns></returns>
        public decimal PosZ()
        {
            return this._posZ;
        }

        /// <summary>
        /// Returns edge length of the object
        /// </summary>
        /// <returns></returns>
        public decimal Edge()
        {
            return this._edge;
        }

        #endregion

        #region Position properties to override

        public abstract decimal MaxLengthX();

        public abstract decimal MinLengthX();

        public abstract decimal MaxLenghtY();

        public abstract decimal MinLengthY();

        public abstract decimal MaxLenghtZ();

        public abstract decimal MinLengthZ();

        #endregion
    }
}
