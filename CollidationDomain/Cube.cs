﻿using System;

namespace CollidationDomain
{
    /// <summary>
    /// Represents a regular hexahedron
    /// </summary>
    public class Cube : CubicCollidationCalculator
    {

        /// <summary>
        /// Constructor of the Cube
        /// </summary>
        /// <param name="posX">Represents the center position on X axis</param>
        /// <param name="posY">Represents the center position on Y axis</param>
        /// <param name="posZ">Represents the center position on z axis</param>
        /// <param name="edgeLenght">Represents the edge length of a cube </param>
        public Cube(decimal posX, decimal posY, decimal posZ, decimal edge)
        {
            this._posX = posX;
            this._posY = posY;
            this._posZ = posZ;
            this._edge = edge;
        }

        public decimal Volume()
        {
            return (decimal)Math.Pow((double)_edge, 3);
        }

        #region Cube position calculation

        public override decimal MaxLengthX()
        {
            return PosX() + Edge() / 2;
        }

        public override decimal MinLengthX()
        {
            return PosX() - Edge() / 2;
        }

        public override decimal MaxLenghtY()
        {
            return PosY() + Edge() / 2;
        }

        public override decimal MinLengthY()
        {
            return PosY() - Edge() / 2;
        }

        public override decimal MaxLenghtZ()
        {
            return PosZ() + Edge() / 2;
        }

        public override decimal MinLengthZ()
        {
            return PosZ() - Edge() / 2;
        }

        #endregion

        #region Cube collidation calculation

        public override bool CollideCubeX(CubicCollidationCalculator anObject)
        {
            return (Math.Abs(anObject.PosX() - PosX()) < (Edge() / 2 + anObject.Edge() / 2));
        }

        public override bool CollideCubeY(CubicCollidationCalculator anObject)
        {
            return (Math.Abs(anObject.PosY() - PosY()) < (Edge() / 2 + anObject.Edge() / 2));
        }

        public override bool CollideCubeZ(CubicCollidationCalculator anObject)
        {
            return (Math.Abs(anObject.PosZ() - PosZ()) < (Edge() / 2 + anObject.Edge() / 2));
        }

        #endregion

    }
}
